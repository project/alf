<?php

/**
 * @file
 * This is a fairly resource hungry filter that parses HTML to append an active
 * class to URL's found in the content. This filter does not cache, limit it's
 * usage to fairly small blocks.
 *
 * The reason for the filter was for theming a HTML list of fixed links, some
 * related to the menu system, and having the need to have the current page
 * highlighted.
 */

/**
 * Implementation of hook_filter_tips().
 */
function alf_filter_tips($delta, $format, $long = FALSE) {
  switch ($delta) {
    case 0:
      if ($long) {
        return t('Parses HTML to discover links to the current page and appends the class attribute with an "%active" class.', array('%active' => _alf_active_class($format)));
      }
      break;
  }
}

function _alf_active_class($format = '') {
  return variable_get("alf_active_class_$format", t('active'));
}

/**
 * Implementation of hook_filter().
 */
function alf_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(t('Active links filter'));
    case 'description':
      return NULL;
    case 'no cache':
      return TRUE;
    case 'prepare':
      return $text;
    case 'process':
      $reg_exp = '/<a[^>]*href=\"([^\"]*)\"[^>]*>/i';
      $replacer = new ALFFilterReplacer($format);
      return preg_replace_callback($reg_exp, array($replacer, 'replacer'), $text);
    case 'settings':
      $form['alf_active_class_'. $format] = array(
        '#type' => 'textfield',
        '#title' => t('Active class'),
        '#default_value' => _alf_active_class($format),
        '#description' => t('The actual class or classes to insert into active links.')
      );
      return $form;
  }
}

class ALFFilterReplacer {
  public $format;
  public static $href = NULL;
  public static $alias = NULL;
  public static $matches = array();

  public static $regexp = array();

  public function __construct($format) {
    global $base_url;
    $this->format = $format;
    if (!isset(self::$href)) {
      $item = menu_get_item();
      self::$href = $item['href'];
      self::$regexp []= str_replace('/', '\\/', preg_quote(self::$href));
      self::$alias = drupal_get_path_alias(self::$href);
      if (self::$alias) {
        self::$regexp []= str_replace('/', '\\/', preg_quote(self::$alias));
      }
    }
  }
  public function replacer($match) {
    if (preg_match('/(' . implode('|', self::$regexp) .')$/', $match[1])) {
      $class = check_plain(_alf_active_class($this->format));
      if (stristr($match[0], 'class')) {
        $match[0] = str_replace('class="', 'class="'. $class .' ', $match[0]);
      }
      else {
        $match[0] = str_replace('">', '" class="'. $class .'">', $match[0]);
      }
    }
    return $match[0];
  }
}
